import 'package:brilliance/common/string.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class InputFieldV1 extends StatefulWidget {
  final TextEditingController? controllerText;
  final String? labelText;
  final String? validationText;
  IconButton? iconButton;
  VoidCallback? function;
  TextInputType? keyboardType;
  Icon? icon;
  bool? obscureText = true;
  InputFieldV1(
      {Key? key,
      this.controllerText,
      this.labelText,
      this.validationText,
      this.function,
      this.keyboardType,
      this.icon,
      this.iconButton,
      this.obscureText})
      : super(key: key);

  @override
  State<InputFieldV1> createState() => _InputFieldV1State();
}

class _InputFieldV1State extends State<InputFieldV1> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: widget.keyboardType,
      controller: widget.controllerText,
      // initialValue: 'Input text',
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(left: 15),
        labelText: widget.labelText,
        labelStyle: TextStyle(
            color: Colors.black87,
            fontFamily: balooDa2,
            fontWeight: FontWeight.w400,
            fontSize: 16),
        // errorText: 'Error message',
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey.shade500,
            width: 1,
          ),
        ),
      ),
      obscureText: widget.obscureText != null
          ? widget.obscureText!
          : widget.obscureText = true,
      validator: (value) {
        if (value!.isEmpty) {
          return widget.validationText;
        } else {
          return null;
        }
      },
    );
  }
}
