import 'package:brilliance/common/string.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class InputField extends StatefulWidget {
  final TextEditingController? controllerText;
  final String? labelText;
  final String? validationText;
  IconButton? iconButton;
  VoidCallback? function;
  TextInputType? keyboardType;
  Icon? icon;
  bool? obscureText = true;
  InputField(
      {Key? key,
      this.controllerText,
      this.labelText,
      this.validationText,
      this.function,
      this.keyboardType,
      this.icon,
      this.iconButton,
      this.obscureText})
      : super(key: key);

  @override
  State<InputField> createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: widget.keyboardType,
      controller: widget.controllerText,
      //cursorColor: Theme.of(context).cursorColor,
      //initialValue: 'Input text',
      // maxLength: 20,
      

      decoration: InputDecoration(
        //icon: Icon(Icons.favorite),
        labelText: widget.labelText,
        labelStyle: TextStyle(
            color: Colors.black87,
            fontFamily: balooDa2,
            fontWeight: FontWeight.w400,
            fontSize: 16),
        //helperText: 'Helper text',
        // suffixIcon: Icon(
        //   Icons.check_circle,
        // ),

        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey.shade400,
            width: 1,
          ),
        ),
      ),

      obscureText: widget.obscureText != null
          ? widget.obscureText!
          : widget.obscureText = true,
      validator: (value) {
        if (value!.isEmpty) {
          return widget.validationText;
        } else {
          return null;
        }
      },
    );
  }
}
