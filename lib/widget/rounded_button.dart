import 'package:brilliance/common/string.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RoundedButton extends StatelessWidget {
  final String buttonText;
  VoidCallback func;
  Color? buttonColor;
  double? width;
  var textColor;

  RoundedButton(
      {Key? key,
      required this.buttonText,
      required this.func,
      this.buttonColor,
      this.width,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      width: width,
      decoration: BoxDecoration(
          color: buttonColor,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: themeColor, width: 1)
          // boxShadow: const [
          //   BoxShadow( color:,
          //     offset: Offset(0,10),)
          // ]
          ),
      child: TextButton(
        onPressed: func,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 0.0),
          child: Text(
            buttonText,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: textColor,
              fontFamily: balooDa2,
            ),
          ),
        ),
      ),
    );
  }
}
