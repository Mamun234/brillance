import 'package:brilliance/common/size_config.dart';
import 'package:brilliance/common/string.dart';
import 'package:brilliance/features/add_deposit/screens/deposit_list.dart';
import 'package:brilliance/features/add_deposit/widget/particular_card.dart';
import 'package:brilliance/widget/input_field_v1.dart';
import 'package:brilliance/widget/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class AddDeposit extends StatefulWidget {
  AddDeposit({Key? key}) : super(key: key);

  @override
  State<AddDeposit> createState() => _AddDepositState();
}

class _AddDepositState extends State<AddDeposit> {
  List listItem = ['Food', 'Lms', 'Gkb'];
  String? valueChoose;

  TextEditingController depoUserNameController = TextEditingController();
  TextEditingController depoNoteController = TextEditingController();

  // TextEditingController depoParticularNameController = TextEditingController();
  // TextEditingController depoQuantityController = TextEditingController();
  // TextEditingController depoAmountController = TextEditingController();

  TextEditingController depoParticularNameModalController =
      TextEditingController();
  TextEditingController depoQuantityModalController = TextEditingController();
  TextEditingController depoAmountModalController = TextEditingController();

  DateTime selectedDate = DateTime.now();
  DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  String? tansactionDateTime;

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2022),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        tansactionDateTime = dateFormat.format(selectedDate);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: themeColor,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(
                context,
                MaterialPageRoute(
                    builder: (context) => const DepositListScreen()));
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            color: Colors.white,
            size: 22,
          ),
        ),
        title: Text(
          "Add deposit",
          style: TextStyle(
              color: Colors.white,
              fontFamily: balooDa2,
              fontWeight: FontWeight.w600,
              fontSize: 18),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                      color: Colors.grey.shade500,
                      width: 1,
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(tansactionDateTime != null
                          ? DateFormat('yyyy-MM-dd')
                              .parse(tansactionDateTime!)
                              .toString()
                          : "${DateTime.now()}"),
                    ),
                    IconButton(
                      onPressed: () {
                        _selectDate(context);
                      },
                      icon: Icon(
                        Icons.date_range_outlined,
                        color: Colors.grey.shade500,
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              InputFieldV1(
                  controllerText: depoUserNameController,
                  validationText: "EnterUser Name",
                  labelText: "User Name",
                  obscureText: false),
              const SizedBox(
                height: 10,
              ),
              InputFieldV1(
                  controllerText: depoNoteController,
                  validationText: "Enter Note",
                  labelText: "Note",
                  obscureText: false),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "Total Amount :",
                      style: TextStyle(
                          color: Colors.black87,
                          fontFamily: balooDa2,
                          fontWeight: FontWeight.w500,
                          fontSize: 18),
                    ),
                    Text(
                      "2000",
                      style: TextStyle(
                          color: Colors.black87,
                          fontFamily: balooDa2,
                          fontWeight: FontWeight.w500,
                          fontSize: 18),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 14,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Particular",
                    style: TextStyle(
                        color: Colors.black87,
                        fontFamily: balooDa2,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                  Text(
                    "Category",
                    style: TextStyle(
                        color: Colors.black87,
                        fontFamily: balooDa2,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                  Text(
                    "Quantity",
                    style: TextStyle(
                        color: Colors.black87,
                        fontFamily: balooDa2,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                  Text(
                    "Amount",
                    style: TextStyle(
                        color: Colors.black87,
                        fontFamily: balooDa2,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return Slidable(
                        key: ValueKey(0),
                        endActionPane: ActionPane(
                          motion: const ScrollMotion(),
                          extentRatio: 0.40,
                          children: [
                            Container(
                                width: 70,
                                decoration: const BoxDecoration(
                                    color: Color(0xFF7BC043)),
                                child: GestureDetector(
                                  onTap: () {
                                    modalView();
                                  },
                                  child: Column(
                                    children: [
                                      const Icon(
                                        Icons.mode_edit_outline_outlined,
                                        size: 20,
                                        color: Colors.white,
                                      ),
                                      Text(
                                        "Edit",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: balooDa2,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12),
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                width: 70,
                                decoration: const BoxDecoration(
                                    color: Color(0xffef5350)),
                                child: GestureDetector(
                                  onTap: () {
                                    print("Tap Delete");
                                  },
                                  child: Column(
                                    children: [
                                      const Icon(
                                        Icons.delete_outlined,
                                        size: 20,
                                        color: Colors.white,
                                      ),
                                      Text(
                                        "Delete",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: balooDa2,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12),
                                      )
                                    ],
                                  ),
                                ))
                          ],
                        ),
                        child: const ParticularCard());
                  }),

              // Card(
              //     //color: Colors.redAccent,
              //     elevation: 3,
              //     shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(10),
              //     ),
              //     child: SizedBox(
              //       width: double.infinity,
              //       child: Padding(
              //         padding: const EdgeInsets.all(12.0),
              //         child: Column(
              //           children: [
              //             InputFieldV1(
              //                 controllerText: depoParticularNameController,
              //                 validationText: "Enter Particular",
              //                 labelText: "Particular",
              //                 obscureText: false),
              //             const SizedBox(
              //               height: 10,
              //             ),
              //             Container(
              //               padding: const EdgeInsets.only(left: 16, right: 16),
              //               decoration: BoxDecoration(
              //                   borderRadius: BorderRadius.circular(7),
              //                   border: Border.all(
              //                     color: Colors.grey,
              //                     width: 1,
              //                   )),
              //               child: DropdownButton(
              //                   value: valueChoose,
              //                   hint: Text("Select Category",
              //                       //'${orderProvider.fetchOrderDetailProvider!.status}',
              //                       style: TextStyle(
              //                           color: Colors.black87,
              //                           fontFamily: balooDa2,
              //                           fontWeight: FontWeight.w400,
              //                           fontSize: 16)),
              //                   isExpanded: true,
              //                   underline: const SizedBox(),
              //                   icon: const Icon(
              //                     Icons.expand_more,
              //                     color: Colors.grey,
              //                   ),
              //                   items: listItem.map((valueItem) {
              //                     return DropdownMenuItem(
              //                       value: valueItem,
              //                       child: Text(valueItem),
              //                     );
              //                   }).toList(),
              //                   onChanged: (newValue) {
              //                     valueChoose = newValue.toString();
              //                   }),
              //             ),
              //             const SizedBox(
              //               height: 10,
              //             ),
              //             Row(
              //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //               children: [
              //                 Container(
              //                   width: SizeConfig.blockSizeHorizontal! * 38.50,
              //                   child: InputFieldV1(
              //                       controllerText: depoQuantityController,
              //                       validationText: "Enter Quantity",
              //                       labelText: "Quantity",
              //                       obscureText: false),
              //                 ),
              //                 Container(
              //                   width: SizeConfig.blockSizeHorizontal! * 38.50,
              //                   child: InputFieldV1(
              //                       controllerText: depoAmountController,
              //                       validationText: "Enter Amount",
              //                       labelText: "Amount",
              //                       obscureText: false),
              //                 ),
              //               ],
              //             ),
              //           ],
              //         ),
              //       ),
              //     )),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Card(
                      elevation: .75,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: GestureDetector(
                        onTap: () {
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (ctx) => AlertDialog(
                              titlePadding: const EdgeInsets.all(0),
                              contentPadding: const EdgeInsets.all(0),
                              title: Container(
                                  height: 35,
                                  color: themeColor,
                                  child: Center(
                                      child: Text(
                                    "Add Particular",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                      fontFamily: balooDa2,
                                    ),
                                  ))),
                              content: SizedBox(
                                width: double.infinity,
                                height: SizeConfig.blockSizeVertical! * 32,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Column(
                                    children: [
                                      InputFieldV1(
                                          controllerText:
                                              depoParticularNameModalController,
                                          validationText: "Enter Particular",
                                          labelText: "Particular",
                                          obscureText: false),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(
                                            left: 16, right: 16),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(7),
                                            border: Border.all(
                                              color: Colors.grey,
                                              width: 1,
                                            )),
                                        child: DropdownButton(
                                            value: valueChoose,
                                            hint: Text("Select Category",
                                                //'${orderProvider.fetchOrderDetailProvider!.status}',
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontFamily: balooDa2,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 16)),
                                            isExpanded: true,
                                            underline: const SizedBox(),
                                            icon: const Icon(
                                              Icons.expand_more,
                                              color: Colors.grey,
                                            ),
                                            items: listItem.map((valueItem) {
                                              return DropdownMenuItem(
                                                value: valueItem,
                                                child: Text(valueItem),
                                              );
                                            }).toList(),
                                            onChanged: (newValue) {
                                              valueChoose = newValue.toString();
                                            }),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            width: SizeConfig
                                                    .blockSizeHorizontal! *
                                                30.76,
                                            child: InputFieldV1(
                                                controllerText:
                                                    depoQuantityModalController,
                                                validationText:
                                                    "Enter Quantity",
                                                labelText: "Quantity",
                                                obscureText: false),
                                          ),
                                          Container(
                                            width: SizeConfig
                                                    .blockSizeHorizontal! *
                                                30.76,
                                            child: InputFieldV1(
                                                controllerText:
                                                    depoAmountModalController,
                                                validationText: "Enter Amount",
                                                labelText: "Amount",
                                                obscureText: false),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  //style: ButtonStyle(textColor: Colors.black87,),

                                  onPressed: () => Navigator.pop(context),
                                  child: Text(
                                    'Cancel',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontFamily: balooDa2,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Container(
                                    height: SizeConfig.blockSizeVertical! * 5,
                                    width:
                                        SizeConfig.blockSizeHorizontal! * 15.40,
                                    decoration: BoxDecoration(
                                        //color: buttonColor,
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(
                                            color: themeColor, width: 1)
                                        // boxShadow: const [
                                        //   BoxShadow( color:,
                                        //     offset: Offset(0,10),)
                                        // ]
                                        ),

                                    //padding: const EdgeInsets.all(8),
                                    child: Center(
                                      child: Text(
                                        "Ok",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: balooDa2,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        child: Container(
                            width: 40,
                            height: 40,
                            child: Icon(
                              Icons.add,
                              size: 32,
                              color: themeColor,
                            )),
                      )),
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedButton(
              buttonText: "Save",
              func: () {
                Navigator.pop(context);
              },
              buttonColor: themeColor,
              width: SizeConfig.blockSizeHorizontal! * 88,
              textColor: Colors.white,
            ),
          ],
        ),
      ),
    );
  }

  Future modalView() {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => AlertDialog(
        titlePadding: const EdgeInsets.all(0),
        contentPadding: const EdgeInsets.all(0),
        title: Container(
            height: 35,
            color: themeColor,
            child: Center(
                child: Text(
              "Add Particular",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: Colors.white,
                fontFamily: balooDa2,
              ),
            ))),
        content: SizedBox(
          width: double.infinity,
          height: SizeConfig.blockSizeVertical! * 32,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                InputFieldV1(
                    controllerText: depoParticularNameModalController,
                    validationText: "Enter Particular",
                    labelText: "Particular",
                    obscureText: false),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.only(left: 16, right: 16),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      border: Border.all(
                        color: Colors.grey,
                        width: 1,
                      )),
                  child: DropdownButton(
                      value: valueChoose,
                      hint: Text("Select Category",
                          //'${orderProvider.fetchOrderDetailProvider!.status}',
                          style: TextStyle(
                              color: Colors.black87,
                              fontFamily: balooDa2,
                              fontWeight: FontWeight.w400,
                              fontSize: 16)),
                      isExpanded: true,
                      underline: const SizedBox(),
                      icon: const Icon(
                        Icons.expand_more,
                        color: Colors.grey,
                      ),
                      items: listItem.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: Text(valueItem),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        valueChoose = newValue.toString();
                      }),
                ),
                const SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: SizeConfig.blockSizeHorizontal! * 30.76,
                      child: InputFieldV1(
                          controllerText: depoQuantityModalController,
                          validationText: "Enter Quantity",
                          labelText: "Quantity",
                          obscureText: false),
                    ),
                    Container(
                      width: SizeConfig.blockSizeHorizontal! * 30.76,
                      child: InputFieldV1(
                          controllerText: depoAmountModalController,
                          validationText: "Enter Amount",
                          labelText: "Amount",
                          obscureText: false),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          TextButton(
            //style: ButtonStyle(textColor: Colors.black87,),

            onPressed: () => Navigator.pop(context),
            child: Container(
                height: SizeConfig.blockSizeVertical! * 4.5,
              width: SizeConfig.blockSizeHorizontal! * 17,
              decoration: BoxDecoration(
                  //color: buttonColor,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: themeColor, width: 1)
                  // boxShadow: const [
                  //   BoxShadow( color:,
                  //     offset: Offset(0,10),)
                  // ]
                  ),
              child: Center(
                child: Text(
                  'Cancel',
                  style: TextStyle(
                      color: Colors.black87,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w400,
                      fontSize: 16),
                ),
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
            },
            child: Container(
             height: SizeConfig.blockSizeVertical! * 4.5,
              width: SizeConfig.blockSizeHorizontal! * 15.40,
              decoration: BoxDecoration(
                  color: themeColor,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: themeColor, width: 1)
                  // boxShadow: const [
                  //   BoxShadow( color:,
                  //     offset: Offset(0,10),)
                  // ]
                  ),

              //padding: const EdgeInsets.all(8),
              child: Center(
                child: Text(
                  "Ok",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w500,
                      fontSize: 16),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
