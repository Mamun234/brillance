import 'package:brilliance/features/add_deposit/screens/add_deposit.dart';
import 'package:brilliance/common/string.dart';
import 'package:brilliance/features/dashboard/screens/dashboard.dart';
import 'package:brilliance/features/add_deposit/widget/deposit_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class DepositListScreen extends StatelessWidget {
  const DepositListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: themeColor,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const DashboardScreen()));
              },
              icon: const Icon(
                Icons.arrow_back_outlined,
                color: Colors.white,
                size: 22,
              ),
            ),
            title: Text(
              "Deposits",
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: balooDa2,
                  fontWeight: FontWeight.w600,
                  fontSize: 18),
            ),
          ),
          body: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: 3,
              itemBuilder: (context, index) {
                return Slidable(
                  key: const ValueKey(0),
                  endActionPane: ActionPane(
                    motion: ScrollMotion(),
                    extentRatio: 0.40,
                    children: [
                      Container(
                          width: 75,
                          height: 98,
                          decoration: const BoxDecoration(
                            color: Color(0xFF7BC043),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0),
                            ),
                          ),
                          child: GestureDetector(
                            onTap: () {
                               Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>  AddDeposit()));
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.mode_edit_outline_outlined,
                                  size: 24,
                                  color: Colors.white,
                                ),
                                const SizedBox(height: 5,),
                                Text(
                                  "Edit",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: balooDa2,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14),
                                )
                              ],
                            ),
                          )),
                      Container(
                          width: 75,
                           height: 98,
                          decoration:
                              const BoxDecoration(color: Color(0xffef5350)),
                          child: GestureDetector(
                            onTap: () {
                              print("Tap Delete");
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.delete_outlined,
                                  size: 24,
                                  color: Colors.white,
                                ),
                                 const SizedBox(height: 5,),
                                Text(
                                  "Delete",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: balooDa2,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14),
                                )
                              ],
                            ),
                          )),
                    ],
                  ),
                  child: Container(
                    color: Colors.white,
                    child: const Padding(
                      padding: EdgeInsets.all(5.0),
                      child: DepositCard(),
                    ),
                  ),
                );
              }),
          floatingActionButton: FloatingActionButton(
              elevation: 1,
              backgroundColor: themeColor,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddDeposit()),
                );
              },
              child: const Icon(
                Icons.add,
                size: 32,
                color: Colors.white,
              ))),
    );
  }
}

void doNothing(BuildContext context) {}
