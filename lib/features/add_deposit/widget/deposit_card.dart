import 'package:brilliance/common/string.dart';
import 'package:flutter/material.dart';

class DepositCard extends StatelessWidget {
  const DepositCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      //color: Colors.redAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Md.Murad",
                  style: TextStyle(
                      color: Colors.black87,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w400,
                      fontSize: 16),
                ),
                Text(
                  "12-03-2022",
                  style: TextStyle(
                      color: Colors.black54,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w400,
                      fontSize: 13),
                ),
              ],
            ),
            const SizedBox(
              height: 7,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Dep No: #0145",
                  style: TextStyle(
                      color: Colors.black54,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w400,
                      fontSize: 14),
                ),
                Icon(
                  Icons.swipe_left_alt_outlined,
                  size: 24,
                  color: Colors.grey.shade500,
                ),
              ],
            ),
            const SizedBox(
              height: 7,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "৳ 1050",
                  style: TextStyle(
                      color: const Color(0xff00c853),
                      //color: Color(0xff4caf50),
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w400,
                      fontSize: 16),
                ),
              ],
            )
          ],
        ),
      )),
    );
  }
}
