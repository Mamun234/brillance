import 'package:brilliance/common/string.dart';
import 'package:flutter/material.dart';

class ParticularCard extends StatelessWidget {
  const ParticularCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
         border: Border.all(
          color: Colors.black12,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Apple",
              style: TextStyle(
                  color: Colors.black87,
                  fontFamily: balooDa2,
                  fontWeight: FontWeight.w400,
                  fontSize: 14),
            ),
             Text(
              "Food",
              style: TextStyle(
                  color: Colors.black87,
                  fontFamily: balooDa2,
                  fontWeight: FontWeight.w400,
                  fontSize: 14),
            ),
             Text(
              "10",
              style: TextStyle(
                  color: Colors.black87,
                  fontFamily: balooDa2,
                  fontWeight: FontWeight.w400,
                  fontSize: 14),
            ),
            Text(
              "400",
              style: TextStyle(
                  color: Colors.black87,
                  fontFamily: balooDa2,
                  fontWeight: FontWeight.w400,
                  fontSize: 14),
            ),
            //  Icon(
            //       Icons.swipe_left_alt_outlined,
            //       size: 24,
            //       color: Colors.grey.shade500,
            //     ),
          ],
        ),
      ),
    );
  }
}
