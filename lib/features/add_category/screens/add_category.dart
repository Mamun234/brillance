import 'package:brilliance/common/size_config.dart';
import 'package:brilliance/common/string.dart';
import 'package:brilliance/features/dashboard/screens/dashboard.dart';
import 'package:brilliance/widget/input_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class CategoryManageScreen extends StatefulWidget {
  const CategoryManageScreen({Key? key}) : super(key: key);

  @override
  State<CategoryManageScreen> createState() => _CategoryManageScreenState();
}

class _CategoryManageScreenState extends State<CategoryManageScreen> {
  TextEditingController categoryNameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: themeColor,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const DashboardScreen()));
            },
            icon: const Icon(
              Icons.arrow_back_outlined,
              color: Colors.white,
              size: 22,
            ),
          ),
          title: Text(
            "Category Manage",
            style: TextStyle(
                color: Colors.white,
                fontFamily: balooDa2,
                fontWeight: FontWeight.w600,
                fontSize: 18),
          ),
        ),
        body: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: 3,
            itemBuilder: (context, index) {
              return Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Slidable(
                    key: const ValueKey(0),
                    endActionPane: ActionPane(
                      motion: const ScrollMotion(),
                      extentRatio: 0.40,
                      children: [
                        // SlidableAction(
                        //   // An action can be bigger than the others.
                        //   flex: 1,
                        //   onPressed: (context) {},
                        //   backgroundColor: Colors.green,
                        //   foregroundColor: Colors.white,
                        //   icon: Icons.edit,
                        //   label: 'Edit',
                        // ),
                        // SlidableAction(
                        //   flex: 1,
                        //   onPressed: (context) {},
                        //   backgroundColor: Colors.redAccent,
                        //   foregroundColor: Colors.white,
                        //   icon: Icons.delete,
                        //   label: 'Delete',
                        // ),
                        Container(
                            width: 76,
                            decoration: const BoxDecoration(
                              color: Color(0xFF7BC043),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                bottomLeft: Radius.circular(10.0),
                              ),
                            ),
                            child: GestureDetector(
                              onTap: () {
                                modalView();
                              },
                              child: Column(
                                children: [
                                  const Icon(
                                    Icons.mode_edit_outline_outlined,
                                    size: 20,
                                    color: Colors.white,
                                  ),
                                  Text(
                                    "Edit",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: balooDa2,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                  )
                                ],
                              ),
                            )),

                        Container(
                            width: 70,
                            decoration:
                                const BoxDecoration(color: Color(0xffef5350)),
                            child: GestureDetector(
                              onTap: () {
                                print("Tap Delete");
                              },
                              child: Column(
                                children: [
                                  const Icon(
                                    Icons.delete_outlined,
                                    size: 20,
                                    color: Colors.white,
                                  ),
                                  Text(
                                    "Delete",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: balooDa2,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                  )
                                ],
                              ),
                            ))
                      ],
                    ),
                    child: Card(
                      elevation: 2,
                      //color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Container(
                          child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Foods",
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: balooDa2,
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                              ),
                            ),
                            const Icon(
                              Icons.swipe_left_alt_outlined,
                              size: 22,
                              color: Colors.black38,
                            ),
                          ],
                        ),
                      )),
                    ),
                  ),
                ),
              );
            }),
        floatingActionButton: FloatingActionButton(
            elevation: 1,
            backgroundColor: themeColor,
            onPressed: () {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (ctx) => AlertDialog(
                  titlePadding: const EdgeInsets.all(0),
                  contentPadding: const EdgeInsets.all(0),
                  title: Container(
                      height: SizeConfig.blockSizeVertical! * 5,
                      color: themeColor,
                      child: Center(
                          child: Text(
                        "Add Category",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                          fontFamily: balooDa2,
                        ),
                      ))),
                  content: SizedBox(
                    width: double.infinity,
                    height: SizeConfig.blockSizeVertical! * 16.70,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        children: [
                          InputField(
                              controllerText: categoryNameController,
                              validationText: "Enter category name",
                              labelText: "Category Name",
                              obscureText: false),
                        ],
                      ),
                    ),
                  ),
                  actions: <Widget>[
                    TextButton(
                      //textColor: Colors.black87,
                      onPressed: () => Navigator.pop(context),
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.black87,
                            fontFamily: balooDa2,
                            fontWeight: FontWeight.w400,
                            fontSize: 16),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(ctx).pop();
                      },
                      child: Container(
                        height: SizeConfig.blockSizeVertical! * 5,
                        width: SizeConfig.blockSizeHorizontal! * 15.40,
                        decoration: BoxDecoration(
                            //color: buttonColor,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: themeColor, width: 1)
                            // boxShadow: const [
                            //   BoxShadow( color:,
                            //     offset: Offset(0,10),)
                            // ]
                            ),

                        //padding: const EdgeInsets.all(8),
                        child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.black87,
                                fontFamily: balooDa2,
                                fontWeight: FontWeight.w500,
                                fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) => AddDeposit()),
              // );
            },
            child: const Icon(
              Icons.add,
              size: 32,
              color: Colors.white,
            )));
  }

  modalView() {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => AlertDialog(
        titlePadding: const EdgeInsets.all(0),
        contentPadding: const EdgeInsets.all(0),
        title: Container(
            height: SizeConfig.blockSizeVertical! * 5,
            color: themeColor,
            child: Center(
                child: Text(
              "Add Category",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: Colors.white,
                fontFamily: balooDa2,
              ),
            ))),
        content: SizedBox(
          width: double.infinity,
          height: SizeConfig.blockSizeVertical! * 16.70,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                InputField(
                    controllerText: categoryNameController,
                    validationText: "Enter category name",
                    labelText: "Category Name",
                    obscureText: false),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          TextButton(
            //textColor: Colors.black87,
            onPressed: () => Navigator.pop(context),
            child: Container(
              height: SizeConfig.blockSizeVertical! * 4.5,
              width: SizeConfig.blockSizeHorizontal! * 17,
              decoration: BoxDecoration(
                  //color: buttonColor,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: themeColor, width: 1)
                  // boxShadow: const [
                  //   BoxShadow( color:,
                  //     offset: Offset(0,10),)
                  // ]
                  ),
              child: Center(
                child: Text(
                  'Cancel',
                  style: TextStyle(
                      color: Colors.black87,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w400,
                      fontSize: 16),
                ),
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
            },
            child: Container(
              height: SizeConfig.blockSizeVertical! * 4.5,
              width: SizeConfig.blockSizeHorizontal! * 15.40,
              decoration: BoxDecoration(
                  color: themeColor,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: themeColor, width: 1)
                  // boxShadow: const [
                  //   BoxShadow( color:,
                  //     offset: Offset(0,10),)
                  // ]
                  ),

              //padding: const EdgeInsets.all(8),
              child: Center(
                child: Text(
                  "Ok",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w500,
                      fontSize: 16),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
