import 'package:brilliance/features/add_expense/screens/add_expense.dart';
import 'package:brilliance/common/string.dart';
import 'package:brilliance/features/dashboard/screens/dashboard.dart';
import 'package:brilliance/features/add_expense/widget/expense_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class ExpenseListScreen extends StatefulWidget {
  const ExpenseListScreen({Key? key}) : super(key: key);

  @override
  State<ExpenseListScreen> createState() => _ExpenseListScreenState();
}

class _ExpenseListScreenState extends State<ExpenseListScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: themeColor,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const DashboardScreen()));
              },
              icon: const Icon(
                Icons.arrow_back_outlined,
                color: Colors.white,
                size: 22,
              ),
            ),
            title: Text(
              "Expenses",
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: balooDa2,
                  fontWeight: FontWeight.w600,
                  fontSize: 18),
            ),
          ),
          body: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: 3,
              itemBuilder: (context, index) {
                return Slidable(
                  key: const ValueKey(0),
                  // The end action pane is the one at the right or the bottom side.
            endActionPane: ActionPane(
                    motion: ScrollMotion(),
                    extentRatio: 0.40,
                    children: [
                      Container(
                          width: 75,
                          height: 98,
                          decoration: const BoxDecoration(
                            color: Color(0xFF7BC043),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0),
                            ),
                          ),
                          child: GestureDetector(
                            onTap: () {
                               Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>  AddExpense()));
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.mode_edit_outline_outlined,
                                  size: 24,
                                  color: Colors.white,
                                ),
                                const SizedBox(height: 5,),
                                Text(
                                  "Edit",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: balooDa2,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14),
                                )
                              ],
                            ),
                          )),
                      Container(
                          width: 75,
                           height: 98,
                          decoration:
                              const BoxDecoration(color: Color(0xffef5350)),
                          child: GestureDetector(
                            onTap: () {
                              print("Tap Delete");
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.delete_outlined,
                                  size: 24,
                                  color: Colors.white,
                                ),
                                 const SizedBox(height: 5,),
                                Text(
                                  "Delete",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: balooDa2,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14),
                                )
                              ],
                            ),
                          )),
                    ],
                  ),
                  child: Container(
                    color: Colors.white,
                    child: const Padding(
                      padding: EdgeInsets.all(5.0),
                      child: ExpenseCard(),
                      
                    ),
                  ),
                );
              }),
          floatingActionButton: FloatingActionButton(
              elevation: 1,
              backgroundColor: themeColor,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddExpense()),
                );
              },
              child: const Icon(
                Icons.add,
                size: 32,
                color: Colors.white,
              ))),
    );
  }
  
}

