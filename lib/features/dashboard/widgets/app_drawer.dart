import 'package:brilliance/features/add_category/screens/add_category.dart';
import 'package:brilliance/features/add_deposit/screens/add_deposit.dart';
import 'package:brilliance/features/add_expense/screens/add_expense.dart';
import 'package:brilliance/common/string.dart';
import 'package:brilliance/features/dashboard/screens/dashboard.dart';
import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
              decoration: BoxDecoration(
                color: themeColor,
              ),
              child: Column(
                children: [
                  const CircleAvatar(
                    radius: 55,
                    //backgroundColor: Colors.white,
                    backgroundImage: AssetImage('assets/images/dimage.png'),
                  ),
                  // Image.asset(
                  //   "assets/images/dimage.png",
                  //   height: 50,
                  //   width: 50,
                  //   fit: BoxFit.fill,
                  // ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Expense Tracker",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: balooDa2,
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                    ),
                  ),
                ],
              )),
          ListTile(
            leading: Icon(
              Icons.dashboard_outlined,
              size: 25,
              color: themeColor,
            ),
            title: Text(
              'Dashboard',
              style: TextStyle(
                color: Colors.black87,
                fontFamily: balooDa2,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const DashboardScreen()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.category_outlined,
              size: 25,
              color: themeColor,
            ),
            title: Text(
              'Add Category',
              style: TextStyle(
                color: Colors.black87,
                fontFamily: balooDa2,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const CategoryManageScreen()));
            },
          ),
          ListTile(
            leading: Image.asset(
              "assets/images/deposit.png",
              height: 25,
              width: 25,
              color: themeColor,
            ),
            title: Text(
              'Add Deposit',
              style: TextStyle(
                color: Colors.black87,
                fontFamily: balooDa2,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AddDeposit()));
            },
          ),
          ListTile(
            leading: Image.asset(
              "assets/images/expense.png",
              height: 25,
              width: 25,
              color: themeColor,
            ),
            title: Text(
              'Add Expense',
              style: TextStyle(
                color: Colors.black87,
                fontFamily: balooDa2,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AddExpense()));
            },
          ),
          const Divider(
            height: 1,
            thickness: 1,
          ),
          // const Padding(
          //   padding: EdgeInsets.all(16.0),
          //   child: Text(
          //     'Label',
          //   ),
          // ),

          ListTile(
            leading: Image.asset(
              "assets/images/list.png",
              height: 25,
              width: 25,
              color: themeColor,
            ),
            title: Text(
              'All Transection',
              style: TextStyle(
                color: Colors.black87,
                fontFamily: balooDa2,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
            onTap: () {
              // Navigator.push(context,
              //     MaterialPageRoute(builder: (context) => AddDeposit()));
            },
          ),

          ListTile(
            leading: Image.asset(
              "assets/images/reports.png",
              height: 25,
              width: 25,
              color: themeColor,
            ),
            title: Text(
              'Reports',
              style: TextStyle(
                color: Colors.black87,
                fontFamily: balooDa2,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
            onTap: () {
              // Navigator.push(context,
              //     MaterialPageRoute(builder: (context) => AddDeposit()));
            },
          ),
        ],
      ),
    );
  }
}
