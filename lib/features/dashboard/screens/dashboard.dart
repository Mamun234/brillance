import 'package:brilliance/common/size_config.dart';
import 'package:brilliance/common/string.dart';
import 'package:brilliance/features/add_deposit/screens/deposit_list.dart';
import 'package:brilliance/features/add_expense/screens/expense_list.dart';
import 'package:brilliance/features/dashboard/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    print(size);
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        backgroundColor: themeColor,
        //leading: SizedBox(width: 0,),
        // IconButton(
        //   onPressed: () {
        //     // Navigator.pop(
        //     //     context,
        //     //     MaterialPageRoute(
        //     //         builder: (context) => const HomeScreen()));
        //   },
        //   icon: const Icon(
        //     Icons.arrow_back_outlined,
        //     color: Colors.white,
        //     size: 22,
        //   ),
        // ),
        title: Text(
          "Dashboard",
          style: TextStyle(
              color: Colors.white,
              fontFamily: balooDa2,
              fontWeight: FontWeight.w600,
              fontSize: 18),
        ),
      ),
      drawer: const AppDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                //height: 300,
                color: Colors.white,
                child: Column(
                  children: [
                    CircularPercentIndicator(
                      animateFromLastPercent: true,
                      backgroundColor: const Color(0xff4caf50),
                      radius: 96.0,
                      lineWidth: 40.0,
                      percent: 19 / 100,
                      center: Container(
                        // height:90,
                        height: 90,
                        width: 90,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                          boxShadow: const [
                            BoxShadow(
                                color: Color(0xff0091ea), spreadRadius: 5),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              // "${ratingAvgPercentage == 0.0 ? 0 : ratingAvgPercentage}%",
                              "E",
                              style: TextStyle(
                                  color:Colors.white,
                                  fontSize: 16,
                                  fontFamily: balooDa2,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ),
                      progressColor: const Color(0xffef5350),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Card(
                      elevation: 2,
                      // color: Colors.amber,
                      child: Container(
                          height: SizeConfig.blockSizeVertical!*12.5,
                          color: Colors.grey.shade50,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          "Deposit",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              //color: Color(0xff4caf50),
                                              fontFamily: balooDa2,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        Text(
                                          "৳ 24467",
                                          style: TextStyle(
                                              color: const Color(0xff00c853),
                                              //color: Color(0xff4caf50),
                                              fontFamily: balooDa2,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        )
                                      ],
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        "Expense",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            //color: Color(0xff4caf50),
                                            fontFamily: balooDa2,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16),
                                      ),
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        "৳ 24467",
                                        style: TextStyle(
                                            color: const Color(0xffef5350),
                                            //color: Color(0xfff44336),
                                            fontFamily: balooDa2,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14),
                                      )
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        "Balance",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            //color: Color(0xff4caf50),
                                            fontFamily: balooDa2,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16),
                                      ),
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        "৳ 24467",
                                        style: TextStyle(
                                            //color: const Color(0xff00b0ff),
                                            color: const Color(0xff0091ea),
                                            fontFamily: balooDa2,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const DepositListScreen()));
                    },
                    child: Card(
                      elevation: 2,
                      color: const Color(0xff00c853),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        height: SizeConfig.blockSizeVertical!*15,
                        width: SizeConfig.blockSizeHorizontal!*42.56,
                        // color: const Color(0xff00c853),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              // height: SizeConfig.blockSizeVertical!*7.18,
                              // width: SizeConfig.blockSizeHorizontal!*14.36,
                               height: 56,
                              width: 56,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(36),
                                  color: Colors.white),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    "assets/images/deposit.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              "Add Deposit",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: balooDa2,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ExpenseListScreen()));
                    },
                    child: Card(
                      elevation: 2,
                      color: const Color(0xffef5350),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        height: SizeConfig.blockSizeVertical!*15,
                        width: SizeConfig.blockSizeHorizontal!*42.56,
                        // color: const Color(0xffef5350),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 56,
                              width: 56,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.white),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    "assets/images/expense.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              "Add Expense",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: balooDa2,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Card(
                      elevation: 2,
                      color: const Color(0xff0091ea),
                      // color: themeColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        height: SizeConfig.blockSizeVertical!*15,
                        width: SizeConfig.blockSizeHorizontal!*42.56,
                        //color: Colors.greenAccent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 56,
                              width: 56,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.white),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    "assets/images/list.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              "Transection",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: balooDa2,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16),
                            )
                          ],
                        ),
                      )),
                  Card(
                    elevation: 2,
                    //color: const Color(0xff0091ea),
                    color: themeColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      height: SizeConfig.blockSizeVertical!*15,
                        width: SizeConfig.blockSizeHorizontal!*42.56,
                      //color: const Color(0xff0091ea),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 56,
                            width: 56,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.white),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  "assets/images/reports.png",
                                  height: 30,
                                  width: 30,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 7,
                          ),
                          Text(
                            "Reports",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: balooDa2,
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
